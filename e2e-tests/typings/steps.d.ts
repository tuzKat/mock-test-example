/// <reference types='codeceptjs' />
type steps_file = typeof import('../settings/i');
type VacancyCard = typeof import('../pages/vacancy-card');
type ListOfVacancies = typeof import('../pages/list-of-vacancies');
type BrowserHelpers = import('../helpers/browser').BrowserHelpers;
type MockRequest = import('@codeceptjs/mock-request');

declare namespace CodeceptJS {
    interface SupportObject {
        I: CodeceptJS.I;
        VacancyCard: VacancyCard;
        ListOfVacancies: ListOfVacancies;
    }
    interface CallbackOrder {
        [0]: CodeceptJS.I;
        [1]: VacancyCard;
        [2]: ListOfVacancies;
    }
    interface Methods extends CodeceptJS.Puppeteer, MockRequest {}
    interface I extends ReturnType<steps_file>, BrowserHelpers {}

    namespace Translation {
        interface Actions {}
    }
}
