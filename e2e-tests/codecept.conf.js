const { MOCK_MODE } = process.env;

exports.config = {
    tests: './cases/e2e/*.e2e.ts',
    output: '../.codeceptjs-output',
    plugins: {
        screenshotOnFail: {
            enabled: true,
        },
    },
    helpers: {
        Puppeteer: {
            url: process.env.TEST_URL,
            waitForTimeout: 3000,
            waitForAction: 200,
            uniqueScreenshotNames: true,
            show: !!process.env.VISUAL,
            restart: false,
            keepBrowserCookie: false,
            keepBrowserState: false,
            userAgent: 'auto-test-vacancies',
            chrome: {
                args: ['--no-sandbox', '--disable-setuid-sandbox', '--window-size=1280,960'],
                defaultViewport: {
                    width: 1280,
                    height: 960,
                },
                executablePath: process.env.CHROMIUM_PATH,
            },
        },
        MockRequestHelper: {
            require: '@codeceptjs/mock-request',
            mode: MOCK_MODE || 'replay',
            logging: false,
            expiresIn: null,
            persisterOptions: {
                keepUnusedRequests: false,
                fs: {
                    recordingsDir: './e2e-tests/data/requests',
                },
            },
            matchRequestsBy: {
                headers: false,
                order: false,
            },
        },
        Browser: {
            require: './helpers/browser',
        },
    },
    include: {
        I: './settings/i.ts',
        VacancyCard: './pages/vacancy-card.ts',
        ListOfVacancies: './pages/list-of-vacancies.ts',
    },
    bootstrap: null,
    teardown: null,
    mocha: {},
    name: 'vacancies',
    require: ['ts-node/register'],
};
